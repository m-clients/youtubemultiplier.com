function redim(type_du_groupe) {
    if (document.body) { var body_larg = document.body.clientWidth; } else { var body_larg = window.innerWidth; }
    if (type_du_groupe == "D") { body_larg = body_larg - 200; }
    var ifplayeryt1, ifplayeryt2, ifplayeryt3, ifplayeryt4, ifplayeryt5, ifplayeryt6, ifplayeryt7, ifplayeryt8;
    ifplayeryt1 = ifplayeryt2 = ifplayeryt3 = ifplayeryt4 = ifplayeryt5 = ifplayeryt6 = ifplayeryt7 = ifplayeryt8 = "vide";
    switch (type_du_groupe) {
        case 'F':
            ifplayeryt8 = document.getElementById('playeryt8');
            ifplayeryt7 = document.getElementById('playeryt7');
        case 'E':
            ifplayeryt6 = document.getElementById('playeryt6');
            ifplayeryt5 = document.getElementById('playeryt5');
        case 'D':
        case 'C':
            ifplayeryt4 = document.getElementById('playeryt4');
        case 'B':
            ifplayeryt3 = document.getElementById('playeryt3'); break; }
    ifplayeryt1 = document.getElementById('playeryt1');
    ifplayeryt2 = document.getElementById('playeryt2');
    var ratio = 0.5625;
    var body_larg_avec_marge = (body_larg - 30);
    if ((type_du_groupe == "A") || (type_du_groupe == "D")) { var new_larg = (body_larg_avec_marge / 2); } else if ((type_du_groupe == "B") || (type_du_groupe == "E")) { var new_larg = (body_larg_avec_marge / 3); } else if ((type_du_groupe == "C") || (type_du_groupe == "F")) { var new_larg = (body_larg_avec_marge / 4); }
    var new_haut = (new_larg * ratio);
    var new_haut = Math.floor(new_haut);
    var new_larg = Math.floor(new_larg);

    function suppr_ajout_haut_larg(player_element, new_largeur, new_hauteur) {
        if (player_element != "vide") { player_element.removeAttribute('width');
            player_element.removeAttribute('height');
            player_element.setAttribute('width', new_largeur);
            player_element.setAttribute('height', new_hauteur); }
    }
    suppr_ajout_haut_larg(ifplayeryt1, new_larg, new_haut);
    suppr_ajout_haut_larg(ifplayeryt2, new_larg, new_haut);
    suppr_ajout_haut_larg(ifplayeryt3, new_larg, new_haut);
    suppr_ajout_haut_larg(ifplayeryt4, new_larg, new_haut);
    suppr_ajout_haut_larg(ifplayeryt5, new_larg, new_haut);
    suppr_ajout_haut_larg(ifplayeryt6, new_larg, new_haut);
    suppr_ajout_haut_larg(ifplayeryt7, new_larg, new_haut);
    suppr_ajout_haut_larg(ifplayeryt8, new_larg, new_haut);
    var tousdiv = document.getElementsByTagName("div");
    for (var j = 0; j < (tousdiv.length); j++) {
        if (tousdiv[j].className == "description_div") { tousdiv[j].style.width = (((new_larg) - 10) + "px"); }
    }
}

function ClearForm(oForm) {
    var form_elements = oForm.elements;
    oForm.reset();
    for (var k = 0; k < (form_elements.length); k++) {
        var type_champ = form_elements[k].type;
        switch (type_champ) {
            case "text":
            case "url":
                form_elements[k].value = '';
                break;
            case "radio":
                if (form_elements[k].checked) { form_elements[k].checked = false; }
                break;
        }
    }
    CheckType('A');
    var perfect_sync = document.getElementById("sync");
    perfect_sync.options[0].selected = true;
    SyncChange();
}

function ChangeColorUrl(inout, type_du_groupe) {
    if (inout == "in") {
        document.getElementById('cellule_type_' + type_du_groupe).style.background = "#CAF775";
        switch (type_du_groupe) {
            case 'F':
                document.getElementById('form_url_8').style.background = "#CAF775";
                document.getElementById('form_start_8').style.background = "#CAF775";
                document.getElementById('form_volume_8').style.background = "#CAF775";
                document.getElementById('form_url_7').style.background = "#CAF775";
                document.getElementById('form_start_7').style.background = "#CAF775";
                document.getElementById('form_volume_7').style.background = "#CAF775";
            case 'E':
                document.getElementById('form_url_6').style.background = "#CAF775";
                document.getElementById('form_start_6').style.background = "#CAF775";
                document.getElementById('form_volume_6').style.background = "#CAF775";
                document.getElementById('form_url_5').style.background = "#CAF775";
                document.getElementById('form_start_5').style.background = "#CAF775";
                document.getElementById('form_volume_5').style.background = "#CAF775";
            case 'D':
            case 'C':
                document.getElementById('form_url_4').style.background = "#CAF775";
                document.getElementById('form_start_4').style.background = "#CAF775";
                document.getElementById('form_volume_4').style.background = "#CAF775";
            case 'B':
                document.getElementById('form_url_3').style.background = "#CAF775";
                document.getElementById('form_start_3').style.background = "#CAF775";
                document.getElementById('form_volume_3').style.background = "#CAF775"; break; }
        document.getElementById('form_url_2').style.background = "#CAF775";
        document.getElementById('form_start_2').style.background = "#CAF775";
        document.getElementById('form_volume_2').style.background = "#CAF775";
        document.getElementById('form_url_1').style.background = "#CAF775";
        document.getElementById('form_start_1').style.background = "#CAF775";
        document.getElementById('form_volume_1').style.background = "#CAF775";
    } else {
        document.getElementById('cellule_type_' + type_du_groupe).style.background = "";
        for (var m = 1; m <= 8; m++) { document.getElementById('form_url_' + m).style.background = "#FFFFFF";
            document.getElementById('form_start_' + m).style.background = "#FFFFFF";
            document.getElementById('form_volume_' + m).style.background = "#FFFFFF"; }
    }
}

function CheckType(type_du_groupe) { document.getElementById('form_type_' + type_du_groupe).checked = true;
    document.getElementById('cellule_type_A').style.color = "";
    document.getElementById('cellule_type_B').style.color = "";
    document.getElementById('cellule_type_C').style.color = "";
    document.getElementById('cellule_type_D').style.color = "";
    document.getElementById('cellule_type_E').style.color = "";
    document.getElementById('cellule_type_F').style.color = "";
    document.getElementById('cellule_type_' + type_du_groupe).style.color = "#FF4263"; }

function ActiveTab(numero_tab) {
    document.getElementById('tab_title_1').removeAttribute('style');
    for (i = 1; i <= 3; i++) {
        if (i == numero_tab) { document.getElementById('tab_content_' + i).style.display = 'block';
            document.getElementById('tab_title_' + i).removeAttribute('onmouseout');
            document.getElementById('tab_title_' + i).style.backgroundColor = '#3a4b71'; } else { document.getElementById('tab_content_' + i).style.display = 'none';
            document.getElementById('tab_title_' + i).removeAttribute('onmouseout');
            document.getElementById('tab_title_' + i).setAttribute('onmouseout', "this.style.backgroundColor='#000000';");
            document.getElementById('tab_title_' + i).style.backgroundColor = '#000000'; }
    }
}

function EnleveCrochet(chaine) { return chaine.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;").replace(/\n/g, "<br />"); }

function SyncChange() {
    var choix = document.getElementById('sync').value;
    if (choix == '1') { document.getElementById('sync-yes').innerHTML = "<span style=\"font-size:xx-small; color: #f08a00;\">A pre-loading function will be automatically applied.</span>"; } else { document.getElementById('sync-yes').innerHTML = ""; }
}

function SaveGroup() {
    document.getElementById('post_group_error').innerHTML = "";
    document.getElementById('HB').style.background = "#FFFFFF";
    document.getElementById('form_titre').style.background = "#FFFFFF";
    var longueur_title = document.getElementById('form_titre').value.length;
    var longueur_captcha = document.getElementById('HB').value.length;
    if (longueur_title < 20) {
        document.getElementById('post_group_error').innerHTML = "<span style=\"color: #C04773;\">We did not find the title of the Mashup.<br />Please, make sure you have put a title between 20 and 100 characters.</span>";
        document.getElementById('form_titre').style.background = "#C04773";
        if (longueur_captcha < 4) { document.getElementById('post_group_error').innerHTML += "<br /><br /><span style=\"color: #C04773;\">You must copy the above figures to prove you're a human.</span>";
            document.getElementById('HB').style.background = "#C04773"; }
    } else {
        if (longueur_captcha < 4) { document.getElementById('post_group_error').innerHTML = "<span style=\"color: #C04773;\">You must copy the above figures to prove you're a human.</span>";
            document.getElementById('HB').style.background = "#C04773"; } else { document.getElementById('post_group_error').innerHTML = "";
            document.getElementById('HB').style.background = "#FFFFFF";
            document.getElementById('form_titre').style.background = "#FFFFFF";
            document.forms['form_videos'].submit(); }
    }
}

function SaveComment() {
    document.getElementById('comment_submit').setAttribute('disabled', "disabled");
    document.getElementById('post_comment_error').innerHTML = "<span style=\"color: #E1A22F;\">... posting comment ...</span>";
    var comment = document.getElementById('comment_text').value;
    var name = document.getElementById('comment_name').value;
    var HBC = document.getElementById('HBC').value;
    var HBBC = document.getElementById('HBBC').value;
    var IDGV = document.getElementById('IDGV').value;
    if (window.XMLHttpRequest) { var xhr = new XMLHttpRequest(); } else if (window.ActiveXObject) { var xhr = new ActiveXObject("Microsoft.XMLHTTP"); } else { document.getElementById('post_comment_error').innerHTML = "<span style=\"color: #C04773;\">You can't leave a comment here because your browser do not support XMLHTTPRequest.</span>"; }
    comment = comment.replace(/&/g, '%26');
    name = name.replace(/&/g, '%26');
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://www.youtubemultiplier.com/save_comment.php', true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.timeout = 4000;
    xhr.send('comment_text=' + comment + '&comment_name=' + name + '&HBC=' + HBC + '&HBBC=' + HBBC + '&IDGV=' + IDGV);
    xhr.onreadystatechange = function attente() {
        if (xhr.readyState == 4) {
            if (xhr.responseText == '<span style="color: #40CE42;">Comment posted</span>') {
                document.getElementById('add_comment').innerHTML = '';
                document.getElementById('post_comment_button').innerHTML = xhr.responseText;
                if (name == '') { name = 'Anonymous'; }
                var comments_tab = document.getElementById('comments_table');
                var nouv_ligne = comments_tab.insertRow(-1);
                var nouv_cellule = nouv_ligne.insertCell(0);
                nouv_cellule.innerHTML = '<strong>' + EnleveCrochet(name) + '</strong> - <span style="color: #888888;">Just now</span><br />' + EnleveCrochet(comment) + '<br />';
                nouv_cellule.setAttribute('style', "background-color: #413318;");
                document.getElementById('no_comments').innerHTML = "";
            } else { document.getElementById('comment_submit').removeAttribute('disabled', "disabled");
                document.getElementById('post_comment_error').innerHTML = xhr.responseText; }
        } else { document.getElementById('post_comment_error').innerHTML = "<span style=\"color: #E1A22F;\">... posting comment ...</span>"; }
    }
}
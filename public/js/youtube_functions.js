var playeryt1;
var playeryt2;
var playeryt3;
var playeryt4;
var playeryt5;
var playeryt6;
var playeryt7;
var playeryt8;
global_ready = new Array('', 0, 0, 0, 0, 0, 0, 0, 0);
global_error = new Array('', 0, 0, 0, 0, 0, 0, 0, 0);
compteur_buffer = 0;
compteur_error_button = 0;

function onYouTubeIframeAPIReady() {
    console.log("onYouTubeIframeAPIReady() called");
    playeryt1 = new YT.Player('playeryt1', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } });
    playeryt2 = new YT.Player('playeryt2', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } });
    if (nb_video > 2) {
        playeryt3 = new YT.Player('playeryt3', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } });
        if (nb_video > 3) {
            playeryt4 = new YT.Player('playeryt4', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } });
            if (nb_video > 4) {
                playeryt5 = new YT.Player('playeryt5', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } });
                if (nb_video > 5) {
                    playeryt6 = new YT.Player('playeryt6', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } });
                    if (nb_video > 6) {
                        playeryt7 = new YT.Player('playeryt7', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } });
                        if (nb_video > 7) { playeryt8 = new YT.Player('playeryt8', { events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange, 'onError': onPlayerError } }) } else { global_ready[8] = 1 }
                    } else { global_ready[7] = global_ready[8] = 1 }
                } else { global_ready[6] = global_ready[7] = global_ready[8] = 1 }
            } else { global_ready[5] = global_ready[6] = global_ready[7] = global_ready[8] = 1 }
        } else { global_ready[4] = global_ready[5] = global_ready[6] = global_ready[7] = global_ready[8] = 1 }
    } else { global_ready[3] = global_ready[4] = global_ready[5] = global_ready[6] = global_ready[7] = global_ready[8] = 1 }
}

function Dark(onoff) {
    if (onoff == "on") { document.getElementById('dark-background').style.display = 'block' } else { document.getElementById('dark-background').style.display = 'none' }
}

function Overlay(onoff) {
    if (onoff == "on") { document.getElementById('overlay-box').style.display = 'block' } else { document.getElementById('overlay-box').style.display = 'none' }
}

function onPlayerReady(event) {
    var player_id = event.target.a.id.substr(8, 1);
    console.log("onPlayerReady() called for player " + player_id);
    if (sync == 1) {
        event.target.playVideo();
        event.target.setVolume(0);
        global_ready[player_id] = 1;
        if ((global_ready[1] == 1) && (global_ready[2] == 1) && (global_ready[3] == 1) && (global_ready[4] == 1) && (global_ready[5] == 1) && (global_ready[6] == 1) && (global_ready[7] == 1) && (global_ready[8] == 1)) { Dark('on');
            Overlay('on');
            Buffer() }
    } else {
        event.target.playVideo();
        var le_volume = volumes[player_id - 1]
        event.target.setVolume(le_volume)
    }
}

function onPlayerError(event) {
    if ((event.data == 100) || (event.data == 101) || (event.data == 150)) { global_error[event.target.id] = 1 }
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED) { event.target.playVideo() }
}

function Buffer() {
    var tab_etat = new Array();
    var tab_charge = new Array();
    var tab_duree = new Array();
    var etat_perso = "null";
    var progress_buffer = 0;
    var timer = 0;
    var animation = "o";
    var tab_temps_charge = new Array(8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8);
    var tab_nom_player = new Array("", playeryt1, playeryt2, playeryt3, playeryt4, playeryt5, playeryt6, playeryt7, playeryt8);
    tab_etat[0] = tab_charge[0] = tab_duree[0] = "";
    document.getElementById('overlay-box').innerHTML = "";
    document.getElementById('overlay-box').innerHTML = "<span style=\"font-size: medium; font-weight: bold; color: #557cd1;\">PRE-LOADING videos for PERFECT SYNCHRONIZATION</span> <button onclick=\"Dark('off'); Overlay('off'); clearTimeout(timer);\">close</button><br />";
    document.getElementById('overlay-box').innerHTML += "<span style=\"font-size: small; font-weight: normal; color: #000000; \">Some AdBlock plugins block the synchronization of Youtube videos.<br />If this window stay empty, try to disable AdBlock and refresh the page.</span><br /><br />";
    for (var i = 1; i <= nb_video; i++) {
        tab_charge[i] = tab_nom_player[i].getVideoLoadedFraction();
        tab_duree[i] = tab_nom_player[i].getDuration();
        if ((tab_duree[i] == 0) || (global_error[i] == 1)) { tab_temps_charge[i] = 8 } else if (tab_start[i] != 0) { tab_temps_charge[i] = (tab_duree[i] * tab_charge[i]) - tab_start[i] } else { tab_temps_charge[i] = tab_duree[i] * tab_charge[i] }
        var duree_petite = tab_duree[i] - tab_start[i];
        if (duree_petite < 7) { var buffer_max = duree_petite } else { var buffer_max = 7 }
        if (tab_temps_charge[i] >= buffer_max) { etat_perso = "<span style=\"color: #40CE42;\">OK - Ready</span>";
            tab_temps_charge[i] = 8 } else if (tab_temps_charge[i] > 0) { etat_perso = "<span style=\"color: #e1a22f;\">Buffering...</span>" } else { etat_perso = "<span style=\"color: #c04773;\">Initializing...</span>" }
        progress_buffer = Math.ceil(tab_temps_charge[i] * 100 / buffer_max);
        if (progress_buffer > 100) { progress_buffer = 100 } else if (progress_buffer < 0) { progress_buffer = "00" } else { progress_buffer = progress_buffer.toString().replace(/^(\d)$/, '0$1') }
        if ((tab_duree[i] == 0) || (global_error[i] == 1)) { progress_buffer = "XX" }
        document.getElementById('overlay-box').innerHTML += "Youtube " + i + ": ";
        document.getElementById('overlay-box').innerHTML += progress_buffer + "% | ";
        document.getElementById('overlay-box').innerHTML += etat_perso + "<br />"
    }
    if (compteur_buffer % 2 == 0) { animation = "o" } else { animation = "x" }
    document.getElementById('overlay-box').innerHTML += "<br />" + animation;
    document.getElementById('overlay-box').innerHTML += "<br /><span style=\"font-style: italic;\">This message will close automatically when all the videos will be ready.</span>";
    document.getElementById('overlay-box').innerHTML += "<br /><span style=\"font-style: italic;\">Videos will restart together from their starting position.</span>";
    document.getElementById('overlay-box').innerHTML += "<br /><br />If necessary, click <button onclick=\"Dark('off'); Overlay('off'); clearTimeout(timer);\">here</button> to stop synchronization and close this windows.";
    if ((tab_temps_charge[1] > 7) && (tab_temps_charge[2] > 7) && (tab_temps_charge[3] > 7) && (tab_temps_charge[4] > 7) && (tab_temps_charge[5] > 7) && (tab_temps_charge[6] > 7) && (tab_temps_charge[7] > 7) && (tab_temps_charge[8] > 7)) {
        clearTimeout(timer);
        Dark('off');
        Overlay('off');
        for (var j = 1; j <= nb_video; j++) { tab_nom_player[j].seekTo(tab_start[j], !0);
            tab_nom_player[j].playVideo();
            tab_nom_player[j].setVolume(volumes[j - 1]) }
    } else { compteur_buffer = compteur_buffer + 1;
        timer = setTimeout(Buffer, 500) }
}

function CheckFunction() {
    if (typeof(playeryt1.playVideo) != "function") {
        if (compteur_error_button % 2 == 0) { document.getElementById('error_buttons').style.color = "#000000" } else { document.getElementById('error_buttons').style.color = "#FFFFFF" }
        document.getElementById('error_buttons').style.background = "#c04773";
        document.getElementById('error_buttons').style.width = "800px";
        document.getElementById('error_buttons').style.margin = "auto";
        document.getElementById('error_buttons').style.marginTop = "20px";
        document.getElementById('error_buttons').style.marginBottom = "40px";
        document.getElementById('error_buttons').style.padding = "8px 8px 8px 8px";
        document.getElementById('error_buttons').innerHTML = "";
        document.getElementById('error_buttons').innerHTML = "<span style=\"font-weight: bold; \">Control buttons are not working?</span><br />Some AdBlock plugins block custom Youtube control buttons.<br /><span style=\"font-weight: bold; \">Try to disable AdBlock and refresh the page.</span>";
        compteur_error_button = compteur_error_button + 1
    } else { document.getElementById('error_buttons').innerHTML = "";
        document.getElementById('error_buttons').style.width = "0px";
        document.getElementById('error_buttons').style.height = "0px" }
}